# README #

This readme document provides whatever steps are necessary and as much as information possible to someone diving in and starting with codebase. 
This will help with onboarding as well as gathering tribal knowledge in case an SME leaves the organization. 
---
### Table of Contents ###
- [User guide](#markdown-header-user-guide)
	- [Information on the Repository](#markdown-header-information-on-the-repository)
- [Installation/Setup Guide](#installation/setup-guide)
- [Build Tools Used](#build-tools-used)
- [Dependencies](#dependencies)
- [Configuration Required](#Configuration-Required)
- [How to run the test](#How-to-run-the-test)
- [Other Tribal Information](#Other-Tribal-Information)
- [Contribution guidelines](#Contribution-guidelines)
- [Who do I talk to?](#Who-do-I-talk-to?)
- [License & Copyright](#License-&-Copyright)




--- ---
# User Guide


# Information on the Repository 

* *Repo Summary*

	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus nisl tincidunt eget nullam non nisi est sit.
	Nunc sed blandit libero volutpat sed cras ornare arcu dui. Enim nunc faucibus a pellentesque sit amet porttitor eget. Venenatis tellus in metus vulputate eu scelerisque felis imperdiet.
	Cursus sit amet dictum sit amet justo donec enim. At augue eget arcu dictum varius duis at consectetur lorem. Vel pharetra vel turpis nunc. Blandit cursus risus at ultrices mi tempus.
	Donec massa sapien faucibus et molestie ac feugiat sed lectus. Tempus quam pellentesque nec nam. Mi ipsum faucibus vitae aliquet. Feugiat in ante metus dictum at tempor. 
	Id cursus metus aliquam eleifend mi in nulla posuere sollicitudin.
  
  
* *Version*
	
	Lorem ipsum version

---
## Installation/Setup Guide 

Scelerisque varius morbi enim nunc faucibus a pellentesque sit amet. Nullam ac tortor vitae purus faucibus ornare suspendisse sed nisi. Eget nunc lobortis mattis aliquam faucibus purus in massa tempor. 
Velit ut tortor pretium viverra suspendisse potenti nullam ac tortor. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Purus sit amet luctus venenatis lectus magna. 
Nulla malesuada pellentesque elit eget gravida cum. Tristique senectus et netus et malesuada fames ac. Donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu. 
Urna nec tincidunt praesent semper feugiat nibh sed pulvinar. Metus aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices. Id aliquet risus feugiat in ante metus dictum at tempor.
Lectus nulla at volutpat diam.
At varius vel pharetra vel turpis nunc eget lorem dolor.

---
## Build Tools Used 

* [Click here to view build tools](#Build-Tools-used)

---

### Dependencies ###

* [Click here to view Dependencies](#Dependencies)

---

### Configuration Required ###

* [Click here to view Compiler configuration information and IDE Notes](#Configuration)

---
### How to run the test ###

1. Step one
2. Step two
3. Step three

---

### Other Tribal Information ###

Sit amet massa vitae tortor condimentum lacinia quis. Lectus proin nibh nisl condimentum id venenatis. In fermentum posuere urna nec tincidunt praesent. Et tortor consequat id porta. 
Est ullamcorper eget nulla facilisi. Sit amet cursus sit amet dictum. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. 
Volutpat odio facilisis mauris sit amet massa vitae tortor condimentum. 
Fringilla phasellus faucibus scelerisque eleifend donec. Eleifend quam adipiscing vitae proin sagittis. Leo in vitae turpis massa sed elementum tempus egestas. 
Sit amet consectetur adipiscing elit.

---

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

---
### Who do I talk to? ###

* Repo owner or admin
* Other Team contact

---
### License & Copyright

Pellentesque elit eget gravida cum sociis natoque penatibus et. Eget arcu dictum varius duis. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique.
Libero id faucibus nisl tincidunt eget nullam non nisi. Etiam dignissim diam quis enim. Facilisi etiam dignissim diam quis enim lobortis scelerisque. Luctus venenatis lectus magna fringilla urna porttitor.
Urna molestie at elementum eu facilisis sed odio morbi quis. Nisi lacus sed viverra tellus in hac habitasse platea dictumst. Sit amet risus nullam eget felis eget nunc lobortis. 
Curabitur vitae nunc sed velit dignissim sodales ut eu sem.
